package com.example.meditationport2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.meditationport2.R
import com.example.meditationport2.feel

class FeelAdapter(val context: Context,val list:ArrayList<feel>):RecyclerView.Adapter<FeelAdapter.MyVH>() {
    class MyVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        val image:ImageView = itemView.findViewById(R.id.image_feel)
        val text2:TextView = itemView.findViewById(R.id.text_feel)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeelAdapter.MyVH {
       val root = LayoutInflater.from(context).inflate(R.layout.feel_adapter,parent,false)
        return MyVH(root)
    }

    override fun onBindViewHolder(holder: FeelAdapter.MyVH, position: Int) {
        holder.image.setImageResource(list[position].image)
        holder.text2.setText(list[position].name)

    }

    override fun getItemCount(): Int {
        return list.size

    }

}