package com.example.meditationport2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.meditationport2.R
import com.example.meditationport2.state

class StateAdapter(val context: Context, val list: ArrayList<state>): RecyclerView.Adapter<StateAdapter.MyVH>()   {
    class MyVH(itemView: View):RecyclerView.ViewHolder(itemView) {
           val images: ImageView = itemView.findViewById(R.id.state_image)
           val text_title: TextView = itemView.findViewById(R.id.title_state)
           val text_descr: TextView = itemView.findViewById(R.id.description_state)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StateAdapter.MyVH {
        val root = LayoutInflater.from(context).inflate(R.layout.state_adapter,parent,false)
        return MyVH(root)
    }

    override fun onBindViewHolder(holder: StateAdapter.MyVH, position: Int) {
       holder.images.setImageResource(list[position].image_state)
       holder.text_title.setText(list[position].title)
       holder.text_descr.setText(list[position].descr)
    }

    override fun getItemCount(): Int {
       return list.size
    }
}