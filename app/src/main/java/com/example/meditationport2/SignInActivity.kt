package com.example.meditationport2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.meditationport2.ui.notifications.NotificationsFragment
import java.util.regex.Pattern.compile

class SignInActivity : AppCompatActivity() {
    lateinit var email: EditText  // поздняя инициализация поля, по идее назвать можно произвольно? Зачем инит вообще?
    lateinit var password: EditText
    lateinit var pattern: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        email = findViewById(R.id.email)  // вот тут привязываем по id
        password = findViewById(R.id.password)
        pattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,50}" +
                "\\@" +
                "[a-zA-Z][a-zA-Z]{0,8}" +
                "(" +
                "\\." +
                "[a-zA-Z][a-zA-Z\\-]{0,5}" +
                ")+"
    }
    fun EmailValid(em:String): Boolean {return compile(pattern).matcher(em).matches()}  // создали функцию сравнения паттерна на совпадения
    fun reg2(view: android.view.View) {
        val intent = Intent(this,SignUpActivity::class.java)
        startActivity(intent)
    }
    fun signin(view: android.view.View) {
        if (email.text.isNotEmpty() && password.text.isNotEmpty()){
            if (EmailValid(email.text.toString())){  // вызываем функцию сравнения, она сверяет текст в email с паттерном
                val menu = Intent(this, MenuActivity::class.java)
                startActivity(menu)
            }
            else{
                val alert = AlertDialog.Builder(this)
                    .setTitle("Ошибка входа")
                    .setMessage("Ошибка Email")
                    .setPositiveButton("Ok",null)
                    .create()
                    .show()
            }

        }
        else {
            val alert = AlertDialog.Builder(this)
                .setTitle("Ошибка входа")
                .setMessage("У Вас есть пустые поля")
                .setPositiveButton("Ok",null)
                .create()
                .show()
 //         Toast.makeText(this, "У Вас есть пустые поля", Toast.LENGTH_SHORT).show()
        }


    }

    fun profile(view: android.view.View){
        val prof = Intent(this, NotificationsFragment::class.java)
        startActivity(prof)
    }

}